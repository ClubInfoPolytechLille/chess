package pieces;
import Board.*;

public class Knight extends Piece {
	//create
	public Knight() {
	}
	public Knight(Position p, boolean color) {
		super(p,color);
	}
	//show
	public char toPGN() {
		return 'N';
	}
	public char toFEN() {
		if(color())
			return 'n';
		return 'N';
	}
 	public String toString() {
		return ":"+super.toString()+":";
	}

	//conditons
	public boolean cheks() {
		return false;
	}
	public boolean isPinned() {
		return false;
	}
	//move
	public boolean moveExist(Move m) {
		return false;
	}
	public boolean movePossible(Move m) {
		return false;
	}
	public void move(Move m) {
		
	}
}
