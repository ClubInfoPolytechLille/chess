package pieces;

import Board.*;

public class Bishop extends Piece {
	//create
	public Bishop() {
		super();
	}
	public Bishop(Position p, boolean color) {
		super(p,color);
	}
	//show
	public char toPGN() {
		return 'B';
	}
	public char toFEN() {
		if(color())
			return 'b';
		return 'B';
	}
	public String toString() {
		return "<"+super.toString()+">";
	}
	//conditions
	public boolean cheks() {
		return false;
	}
	public boolean isPinned() {
		return false;
	}
	//move
	public boolean moveExist(Move m) {
		return false;
	}
	public boolean movePossible(Move m) {
		return false;
	}
	public void move(Move m) {
		
	}
}
