package pieces;
import Board.*;

public abstract class Piece {
	private Position pos;
	private boolean moved;
	private boolean color;//0 is white, 1 is black
	
	
	//create
	public Piece() {
		pos=new Position();
		color=true;
		moved=false;
	}
	public Piece(Position p,boolean color) {
		pos=p;
		this.color=color;
		moved=false;
	}
	public static Piece createPiece(Position p,char piece_fen) throws WrongPieceFENException{
		//System.err.println("create piece : "+piece_fen);
		boolean color=Character.isLowerCase(piece_fen);
		piece_fen=Character.toLowerCase(piece_fen);
		switch(piece_fen) {
			case 'b':return (Piece)new Bishop(p,color);
			case 'k':return (Piece)new King(p,color);
			case 'n':return (Piece)new Knight(p,color);
			case 'p':return (Piece)new Pawn(p,color);
			case 'q':return (Piece)new Queen(p,color);
			case 'r':return (Piece)new Rook(p,color);
			default : throw new WrongPieceFENException();
		}
	}
	
	//get set
	public int getRank() {
		return pos.getRank();
	}
	public int getColumn() {
		return pos.getColumn();
	}	
	public Position getPosition() {
		return pos;
	}
 	public boolean color() {
		return color;
	}
	public Position getPos() {
		return pos;
	}
	public boolean yetMoved() {
		return moved;
	}
	public void setPos(Position pos) {
		this.pos = pos;
	}
	public void setMoved(boolean moved) {
		this.moved = moved;
	}
	public void setRank(int r) {
		pos.setRank(r);
	}
	public void setColumn(int column) {
		pos.setColumn(column);
	}
	public void setColor(boolean color) {
		this.color = color;
	}
	

	//show
	public abstract char toPGN();
	public abstract char toFEN();
	public String toString() {
		if(color)
			return"O";
		return"X";
	}
	
	
	//kind
	public boolean isKing() {
		return false;
	}
	public boolean isPawn() {
		return false;
	}
	
	
	//move
	public abstract boolean moveExist(Move m);
	public abstract boolean movePossible(Move m);
	public abstract void move(Move m);
	
	
	//checks
	public abstract boolean cheks();
	public abstract boolean isPinned();
}
/*I
black  O
white  X

king    "(X)"  --- "(O)"
queen   "|X|"  --- "|O|"
bishop  "<X>"  --- "<O>"
knight  ":X:"  --- ":O:"
rook    "]X["  --- "]O["
pawn    " X "  --- " O "

place
dark
~~~~~~~~~~~~~~~~~~~~~~~   
|
|
|
========"""""''''���!!���3333####
light
|#####
|#   #
|#####
~~~~~~

=======
*/