package pieces;

import Board.*;

public class Rook extends Piece {
	//create
	public Rook() {
		super();
	}
	public Rook(Position p,boolean color) {
		super(p,color);
	}
	//show
	public char toPGN() {
		return 'R';
	}
	public char toFEN() {
		if(color())
			return 'r';
		return 'R';
	}
	public String toString() {
		return "]"+super.toString()+"[";
	}
	//conditions
	public boolean cheks() {
		return false;
	}
	public boolean isPinned() {
		return false;
	}
	//move
	public boolean moveExist(Move m) {
		return false;
	}
	public boolean movePossible(Move m) {
		return false;
	}
	public void move(Move m) {
		
	}

}
