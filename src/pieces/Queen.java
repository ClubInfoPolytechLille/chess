package pieces;

import Board.*;

public class Queen extends Piece {
	//create
	public Queen() {
		super();
	}
	public Queen(Position p, boolean color) {
		super(p,color);
	}
	
	
	//show
	public char toPGN() {
		return 'Q';
	}
	public char toFEN() {
		if(color())
			return 'q';
		return 'Q';
	}
	public String toString() {
		return "|"+super.toString()+"|";
	}
	
	
	//conditions
	public boolean cheks() {
		return false;
	}
	public boolean isPinned() {
		return false;
	}
	
	
	//move
	public boolean moveExist(Move m) {

		return false;
	}
	public boolean movePossible(Move m) {

		return false;
	}
	public void move(Move m) {

		
	}

}
