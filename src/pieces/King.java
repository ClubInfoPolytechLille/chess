package pieces;

import Board.*;
import java.lang.Math;

public class King extends Piece {
	//create
	public King() {
		super();
	}
	public King(Position p,boolean color) {
		super(p,color);
	}
	
	
	//show
	public char toPGN() {
		return 'K';
	}
	public char toFEN() {
		if(color())
			return 'k';
		return 'K';
	}
	/*can be changed to
	 * public char toChar(boolean forFEN){
	 * if(color()&&forFen)
	 *	return 'k';
	 * return 'K';
	 */
	public String toString() {
		return "("+super.toString()+")";
	}
	
	
	//conditions
	public boolean isKing() {
		return true;
	}
	public boolean cheks() {
		return false;
	}
	public boolean isPinned() {
		return false;
	}
	
	
	//move
	public boolean moveExist(Move m) {
		boolean b=m.getIni().getRank()==m.getFin().getRank();
		if(!this.yetMoved()&&b) {
			
		}
		b=(Math.abs(m.getIni().getRank()-m.getFin().getRank())<1 && Math.abs(m.getIni().getColumn()-m.getFin().getColumn())<1);
		return b;
	}
	public boolean movePossible(Move m) {
		return false;
	}
	public void move(Move m) {
		
		this.setMoved(false);
	}

}
