package pieces;

import Board.*;

public class Pawn extends Piece {
	//create
	public Pawn() {
		super();
	}
	public Pawn(Position p, boolean color) {
		super(p,color);
	}
	
	
	//show
	public char toPGN() {
		return 'p';
	}
	public char toFEN() {
		if(color())
			return 'p';
		return 'P';
	}
	public String toString() {
		return " "+super.toString()+" ";
	}
	
	
	//conditions
	public boolean isPawn() {
		return true;
	}
	public boolean cheks() {
		return false;
	}
	public boolean isPinned() {
		return false;
	}

	//move
	public boolean moveExist(Move m) {
		return false;
	}
	public boolean movePossible(Move m) {
		return false;
	}
	public void move(Move m) {
		
	}

}
