package Board;
//import java.util.ArrayList;
import pieces.*;

public class Board {
	private Team teams[];
	private Places places[][];//[c][r]
	private PGN game;
	private FEN start,current;
	private final String iLine="\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~";

	//create
	public Board() throws WrongFENException{
		game=new PGN();

		FEN strt=new FEN();
		Team[] tmpTeams=new Team[2];tmpTeams[0]=new Team(false);tmpTeams[1]=new Team(true);
		places=new Places[8][8];
		int r,c;
		Piece User;
		for(r=8;r>0;r--) {
			//System.err.println("loop 1 : "+r);
			String Users=strt.getPos()[8-(r)];
			for(c=1;c<=8;c++) {
				//System.err.println("\tloop 2 : "+c);
				User=null;
				char c_user;
				try {
					c_user=Users.charAt(0);
				}catch(StringIndexOutOfBoundsException sioob) {
					throw new WrongFENException("not enough char for rank "+r);
				}
				if(Character.isDigit(c_user)) {
					//System.err.println("digit");
					Users=(c_user=='1' ? "" :
						Character.toString(c_user-1)  ) 
						+ Users.subSequence(1,Users.length());
				}
				else {
					try {
						//System.err.println("go to create piece");
						User=Piece.createPiece(new Position(r,c),c_user);
					}catch(WrongPieceFENException wpf) {
						throw new WrongFENException("not recognized char");
					}
					//System.err.println("p:" + User.getPosition() +" - "+ User.getClass());
					tmpTeams[(User.color()? 1:0)].addPiece(User);
					Users=(String)Users.subSequence(1,Users.length());
				}
				places[c-1][r-1]=
						new Places(new Position(r,c),User);
				}
			start=strt;
			current=strt;
			teams=tmpTeams;
		}
	}
	public String toString() {
		String all=iLine,line;int r,i,c;
		for(r=8;r>0;r--) {
			for(i=0;i<3;i++) {
				line="\n";
				for(c=0;c<8;c++) {
					line+=(places[c][r-1]).toString(i);
				}
				line+="|";
				all+=line;
			}
			all+=iLine;
		}
		return all;
	}
	
	
	//get set
	public Team[] getTeams() {
		return teams;
	}
	public Places[][] getPlaces() {
		return places;
	}
	public Places getPlace(Position p) {		
		return places[p.getColumn()-1][p.getRank()-1];
	}
	public PGN getGame() {
		return game;
	}
	public FEN getStart() {
		return start;
	}
	public FEN getCurrent() {
		return current;
	}
	
	public void setTeams(Team[] teams) {
		this.teams = teams;
	}
	public void setPlaces(Places[][] places) {
		this.places = places;
	}
	public void setStart(FEN start){
		
	}
}
