package Board;

public class WrongFENException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public WrongFENException() {super();}
	public WrongFENException(String msg) {super(msg);}
}
