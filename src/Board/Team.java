package Board;
import pieces.*;
import java.util.Enumeration;
import java.util.Hashtable;

public class Team {
	private Name name;
	private Hashtable<Position,Piece> icons;
	//create
	public Team(Name n) {
		name=n;
		icons=new Hashtable<Position,Piece>();
	}
	public Team(boolean color) {
		this(color? Name.Black : Name.White );
	}
	
	
	//get set
	public Name getName() {
		return name;
	}
	public Hashtable<Position, Piece> getIcons() {
		return icons;
	}
	public Piece getIcon(Position pos) {
		return icons.get(pos);
	}
	
	public void setName(Name name) {
		this.name = name;
	}
	public void setIcons(Hashtable<Position,Piece> icons) {
		this.icons = icons;
	}
	
	
	//
	public Position getKingp() {
		Enumeration<Piece> ps=icons.elements();
		for(;ps.hasMoreElements();) {
			if(ps.nextElement().isKing())
				return ps.nextElement().getPosition();
		}
		return null;
	}
	public void addPiece(Piece p) {
		icons.put(p.getPos(), p);
	}
}
