package Board;
import java.lang.Character;

public class Position {
	private static final int min=1,max=8;
	private int rank,column;
	
	//create
	public Position() {
		rank=-1;
		column=-1;
	}
	public Position(int r,int c) {
		rank=r;column=c;
	}
	public Position(int r,char c) {
		this(r,c-'a');
	}
	public Position(char r,char c) {
		this(r-'0',c-'a');
	}
	public String toString() {
		return Character.toString((char)('a'+column-1))+rank;
	}
	
	
	// get set
	public static int getMin() {
		return min;
	}
	public static int getMax() {
		return max;
	}
	
	public int getRank() {
		return rank;
	}
	public int getColumn() {
		return column;
	}
	public void setRank(int rank) {
		this.rank = rank;
	}
	public void setColumn(int column) {
		this.column = column;
	}
	
}
