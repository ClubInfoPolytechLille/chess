package Board;


public class FEN {
	private String[] pos;
	private String castle;
	private char nextplay, enPassant;
	private int lastPawnMove,moveNumber;
	
	//define some positions
	private static String[][] startingPos= {
			{"rnbqkbnr","pppppppp","8","8","8","8","PPPPPPPP","RNBQKBNR"},	//classic
			{"8","8","8","8","8","8","krnbBNRK","qrnbBNRQ"},				//racing kings
			};
	 
	
	//create
	public FEN() {
		pos=startingPos[0];
		castle="KQkq";
		nextplay='w';
		enPassant='-';
		lastPawnMove=0;
		moveNumber=1;
	}
	public String toString(){
		String s=pos[0];
		for(int i=1;i<8;i++)
			s+="/"+pos[i];
		s+=" "+castle +" "+nextplay +" "+enPassant +" "+lastPawnMove +" "+moveNumber;
		return s;
	}	
	
	//get set
	public String[] getPos() {
		return pos;
	}
	public String getCastle() {
		return castle;
	}
	public char getNextplay() {
		return nextplay;
	}
	public char getEnPassant() {
		return enPassant;
	}
	public Position getEnPassantPos() {
		return new Position(nextplay=='w'?6:3,enPassant);
	}
	public int getLastPawnMove() {
		return lastPawnMove;
	}
	public int getMoveNumber() {
		return moveNumber;
	}
	
	public void setPos(String[] pos) {
		this.pos = pos;
	}
	public void setCastle(String castle) {
		this.castle = castle;
	}
	public void setNextplay(char nxtplay) {
		this.nextplay = nxtplay;
	}
	public void setEnPassant(char enPassant) {
		this.enPassant = enPassant;
	}
	public void setLastPawnMove(int lastPawnMove) {
		this.lastPawnMove=lastPawnMove;
	}
	public void setMoveNumber(int moveNumber) {
		this.moveNumber=moveNumber;
	}
	
	//
	public void resetLastPawnMove() {
		lastPawnMove=0;
	}
	public void increaseLastPawnMove() {
		lastPawnMove++;
	}
	public void increaseMoveNumber() {
		moveNumber++;
	}
	public void resetEnPassant() {
		enPassant='-';
	}

}
