package Board;
import pieces.*;

public class Move {
	private Position ini,fin;
	private Board board;
	//create
	public Move(Board b,Position i,Position f) {
		ini=i;
		fin=f;
		board=b;
	}
	public String toString() {
		String s=getPieceMoving().toPGN()+ini.toString();
		if(takes())s+="x";
		return s+fin;
	}
	
	
	//get set
	public Position getIni() {
		return ini;
	}
	public Position getFin() {
		return fin;
	}
	public Board getBoard() {
		return board;
	}
	
	public void setIni(Position ini) {
		this.ini = ini;
	}
	public void setFin(Position fin) {
		this.fin = fin;
	}
	public void setBoard(Board board) {
		this.board = board;
	}
	
	
	//specials
	public Piece getPiece(boolean take) {
		if(take)return getPieceMoving();
		return getPieceTaken();
	}
	public Piece getPieceMoving() {
		return board.getPlace(ini).getUser();
	}
	public boolean takes() {
		return board.getPlace(fin).isUsed();
	}
	public Piece getPieceTaken() {
		return board.getPlace(fin).getUser();
	}
	
}
