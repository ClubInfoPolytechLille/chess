package Board;
import pieces.*;

public class Places {
	private Position pos;
	private boolean shine;//0 is dark,1 is light
	private Piece user;
	
	//create
	public Places() {
		pos=new Position();
		shine=!((pos.getRank()%2)==(pos.getColumn()%2));
		user=null;
	}
	public Places(Position p) {
		pos=p;
		shine=!((pos.getRank()%2)==(pos.getColumn()%2));
		user=null;
	}
	public Places(Position p,Piece User) {
		pos=p;
		shine=!((pos.getRank()%2)==(pos.getColumn()%2));
		user=User;
	}
	
	public String toString() {
		return super.toString()+ ": "+ pos +" : "+ shine +" : "+ user;
	}
	public String toString(int l){
		String s="|";
		if(isUsed() && l==1) {
			if(shine)s+="#"+user+"#";
			else s+=" "+user+" ";
			}		
		else if(shine)s+="#####";
			 else     s+="     ";
		return s;
	}
	
	//necessary methods
	public static int intValue(char c) {
		if(Character.isDigit(c))
			return c-'0';
		return -1;
	}

	// get set
 	public int getRank() {
		return pos.getRank();
	}
	public int getColumn() {
		return pos.getColumn();
	}
	public boolean Shine() {
		return shine;
	}
	public boolean isUsed() {
		return user!=null;
	}
	public Piece getUser() {
		return user;
	}
	public void setRank(int r) {
		pos.setRank(r);
	}
	public void setColumn(int c) {
		pos.setColumn(c);
	}
	public void setPos(String p) {
		pos.setColumn(p.charAt(0)-'A');
		pos.setRank(intValue(p.charAt(1)));
	}
	public void setShine() {
		shine = !((pos.getRank()%2)==(pos.getColumn()%2));
	}
	public void setUser(Piece user) {
		this.user = user;
	}

	//other methods
}